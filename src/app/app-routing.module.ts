import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './authentication/register/register.component';

import { AuthenticationRoutingModule } from './authentication/authentication-routing.module';
const routes: Routes = [
  { path: '', component: RegisterComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), AuthenticationRoutingModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
