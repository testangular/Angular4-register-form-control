import { Component } from '@angular/core';

@Component({
  selector: 'infoandtech-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'infoandtech';
}
