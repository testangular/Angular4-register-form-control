import { ValidatorPasswordDirective } from './validator-password.directive';

import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

describe('ValidatorPasswordDirective', () => {
  it('should create an instance', () => {
    const directive = new ValidatorPasswordDirective('validatorEqual', 'reverse');
    expect(directive).toBeTruthy();
  });
});
