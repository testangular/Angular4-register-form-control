import { Component, OnInit, ViewChild, Output, EventEmitter} from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'infoandtech-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @ViewChild(NgForm)
  registerForm: NgForm;
  @Output() registrationService = new EventEmitter<string>();

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }
  onValidate() {
    if (this.registerForm.valid) {
       console.log(this.registerForm.value);
      this.http.post(environment.backendUrl + 'api/register', this.registerForm.value)
      // retour appel HTTP
      .subscribe(
      data => {
        this.registrationService.emit(data['userDTO']);
      },
      err => {
        this.registrationService.emit('ERROR');
      });
    }
  }
}
