import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { RegisterComponent } from './register/register.component';
import { ValidatorPasswordDirective } from './validator-password.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AuthenticationRoutingModule,
    HttpClientModule
  ],
  declarations: [RegisterComponent, ValidatorPasswordDirective],
  exports: [
    RegisterComponent
  ],
  providers: [
  ]
})
export class AuthenticationModule { }
